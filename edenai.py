import json
import requests

headers = {"Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiODJmMWRlNDUtYjUzOS00ZGFkLWE5MWYtYWZmNTU1NmNmNzUyIiwidHlwZSI6ImFwaV90b2tlbiJ9.JtN_Mg9d4AL62PGu7cLUbYv99CyFSl7i6Qr9w9OEGMs"}

url = "https://api.edenai.run/v2/text/chat"
payload = {
    "providers": "openai",
    "text": "Where is the princess to rescue?",
    "chatbot_global_action": "You are a hunter in a fantasy world",
    "previous_history": [],
    "temperature": 0.0,
    "max_tokens": 150,
    "fallback_providers": ""
}

response = requests.post(url, json=payload, headers=headers)

result = json.loads(response.text)
print(result['openai']['generated_text'])
