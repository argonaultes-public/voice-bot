const apiKey = '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';
const url = 'https://api.anthropic.com/v1/complete';

const data = {
    model: 'claude-2',
    prompt: '\n\nHuman:What does sell LDLC?\n\nAssistant:',
    max_tokens_to_sample: 20, // Adjust for desired output length
};

const headers = {
    'x-api-key': apiKey,
    'Content-Type': 'application/json',
    'anthropic-version': '2023-06-01',
};

fetch(url, { method: 'POST', headers, body: JSON.stringify(data) })
    .then((response) => response.json())
    .then((data) => {
        console.log('Response data: ', data);
        console.log('Generated Text:', data.completions[0].text);
    })
    .catch((error) => console.error(error));